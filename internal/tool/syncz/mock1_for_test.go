// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz (interfaces: Worker)
//
// Generated by this command:
//
//	mockgen -typed -destination mock1_for_test.go -package syncz gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz Worker
//

// Package syncz is a generated GoMock package.
package syncz

import (
	context "context"
	reflect "reflect"

	gomock "go.uber.org/mock/gomock"
)

// MockWorker is a mock of Worker interface.
type MockWorker struct {
	ctrl     *gomock.Controller
	recorder *MockWorkerMockRecorder
}

// MockWorkerMockRecorder is the mock recorder for MockWorker.
type MockWorkerMockRecorder struct {
	mock *MockWorker
}

// NewMockWorker creates a new mock instance.
func NewMockWorker(ctrl *gomock.Controller) *MockWorker {
	mock := &MockWorker{ctrl: ctrl}
	mock.recorder = &MockWorkerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockWorker) EXPECT() *MockWorkerMockRecorder {
	return m.recorder
}

// Run mocks base method.
func (m *MockWorker) Run(arg0 context.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Run", arg0)
}

// Run indicates an expected call of Run.
func (mr *MockWorkerMockRecorder) Run(arg0 any) *MockWorkerRunCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Run", reflect.TypeOf((*MockWorker)(nil).Run), arg0)
	return &MockWorkerRunCall{Call: call}
}

// MockWorkerRunCall wrap *gomock.Call
type MockWorkerRunCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockWorkerRunCall) Return() *MockWorkerRunCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockWorkerRunCall) Do(f func(context.Context)) *MockWorkerRunCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockWorkerRunCall) DoAndReturn(f func(context.Context)) *MockWorkerRunCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
