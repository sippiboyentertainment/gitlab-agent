// Mocks for Gitaly.
package mock_gitaly

//go:generate mockgen.sh -destination "gitaly.go" -package "mock_gitaly" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly/vendored/gitalypb" "CommitServiceClient,CommitService_TreeEntryClient,CommitService_GetTreeEntriesClient"
