package grpctool

import (
	"context"
	"crypto/ed25519"

	"github.com/golang-jwt/jwt/v5"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/auth"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type JWTAuther struct {
	issuer            string // may be empty to disable validation
	audience          string // may be empty to disable validation
	validMethods      []string
	loggerFromContext func(context.Context) *zap.Logger
	keyFunc           jwt.Keyfunc
}

func NewHMACJWTAuther(secret []byte, issuer, audience string, loggerFromContext func(context.Context) *zap.Logger) *JWTAuther {
	return &JWTAuther{
		issuer:            issuer,
		audience:          audience,
		validMethods:      []string{"HS256", "HS384", "HS512"},
		loggerFromContext: loggerFromContext,
		keyFunc: func(token *jwt.Token) (any, error) {
			return secret, nil
		},
	}
}

func NewEdDSAJWTAuther(publicKey ed25519.PublicKey, issuer, audience string, loggerFromContext func(context.Context) *zap.Logger) *JWTAuther {
	return &JWTAuther{
		issuer:            issuer,
		audience:          audience,
		validMethods:      []string{"EdDSA"},
		loggerFromContext: loggerFromContext,
		keyFunc: func(token *jwt.Token) (any, error) {
			return publicKey, nil
		},
	}
}

// UnaryServerInterceptor returns a new unary server interceptors that performs per-request JWT auth.
func (a *JWTAuther) UnaryServerInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	if err := a.doAuth(ctx); err != nil {
		return nil, err
	}
	return handler(ctx, req)
}

// StreamServerInterceptor returns a new stream server interceptors that performs per-request JWT auth.
func (a *JWTAuther) StreamServerInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	if err := a.doAuth(stream.Context()); err != nil {
		return err
	}
	return handler(srv, stream)
}

func (a *JWTAuther) doAuth(ctx context.Context) error {
	token, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return err // returns gRPC status error
	}
	_, err = jwt.Parse(
		token,
		a.keyFunc,
		jwt.WithAudience(a.audience),
		jwt.WithIssuer(a.issuer),
		jwt.WithValidMethods(a.validMethods),
	)
	if err != nil {
		a.loggerFromContext(ctx).Debug("JWT validation failed", logz.Error(err))
		return status.Error(codes.Unauthenticated, "JWT validation failed")
	}
	return nil
}
