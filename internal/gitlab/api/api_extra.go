package api

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
)

func (a *GetAgentInfoResponse) ToAPIAgentInfo() *api.AgentInfo {
	return &api.AgentInfo{
		ID:            a.AgentId,
		ProjectID:     a.ProjectId,
		Name:          a.AgentName,
		GitalyInfo:    a.GitalyInfo,
		Repository:    a.GitalyRepository.ToGitalyRepository(),
		DefaultBranch: a.DefaultBranch,
	}
}

func (p *GetProjectInfoResponse) ToAPIProjectInfo() *api.ProjectInfo {
	return &api.ProjectInfo{
		ProjectID:     p.ProjectId,
		GitalyInfo:    p.GitalyInfo,
		Repository:    p.GitalyRepository.ToGitalyRepository(),
		DefaultBranch: p.DefaultBranch,
	}
}
