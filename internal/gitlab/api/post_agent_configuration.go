package api

import (
	"context"
	"net/http"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
)

const (
	AgentConfigurationAPIPath = "/api/v4/internal/kubernetes/agent_configuration"
)

func PostAgentConfiguration(ctx context.Context, client gitlab.ClientInterface, agentID int64,
	config *agentcfg.ConfigurationFile, opts ...gitlab.DoOption) error {
	return client.Do(ctx,
		joinOpts(opts,
			gitlab.WithMethod(http.MethodPost),
			gitlab.WithPath(AgentConfigurationAPIPath),
			gitlab.WithJWT(true),
			gitlab.WithProtoJSONRequestBody(&AgentConfigurationRequest{
				AgentId:     agentID,
				AgentConfig: config,
			}),
			gitlab.WithResponseHandler(gitlab.NoContentResponseHandler()),
		)...,
	)
}
