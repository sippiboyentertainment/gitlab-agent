package agent

import (
	"sync"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/info"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunclient"
	"google.golang.org/grpc"
)

const (
	minIdleConnections = 2
	maxConnections     = 500
	maxIdleTime        = time.Minute
	// scaleUpStep defines how many new connections are started when there is not enough idle connections.
	scaleUpStep = 10

	connectionInitBackoff   = 1 * time.Second
	connectionMaxBackoff    = 20 * time.Second
	connectionResetDuration = 25 * time.Second
	connectionBackoffFactor = 1.6
	connectionJitter        = 0.2
)

type Factory struct {
	APIServerConn grpc.ClientConnInterface
}

func (f *Factory) IsProducingLeaderModules() bool {
	return false
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	rtClient := rpc.NewReverseTunnelClient(config.KASConn)
	pollConfig := retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
		connectionInitBackoff,
		connectionMaxBackoff,
		connectionResetDuration,
		connectionBackoffFactor,
		connectionJitter,
	))
	descriptor := sync.OnceValue(func() *info.APIDescriptor {
		return tunclient.APIDescriptor(config.APIServer)
	})
	return &module{
		cm: tunclient.NewConnectionManager(
			minIdleConnections,
			maxConnections,
			scaleUpStep,
			maxIdleTime,
			func(onActive, onIdle func(c tunclient.ConnectionInterface)) tunclient.ConnectionInterface {
				return &tunclient.Connection{
					Log:           config.Log,
					Descriptor:    descriptor(),
					Client:        rtClient,
					OwnServerConn: f.APIServerConn,
					PollConfig:    pollConfig,
					OnActive:      onActive,
					OnIdle:        onIdle,
				}
			},
		),
	}, nil
}

func (f *Factory) Name() string {
	return agentk2kas_tunnel.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartAfterServers
}
