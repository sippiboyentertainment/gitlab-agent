package router

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	otelcodes "go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type findTunnelRequest struct {
	agentID         int64
	service, method string
	retTun          chan<- *tunserver.TunnelImpl
}

type findHandle struct {
	tracer    trace.Tracer
	retTun    <-chan *tunserver.TunnelImpl
	done      func(context.Context)
	gotTunnel bool
}

func (h *findHandle) Get(ctx context.Context) (tunserver.Tunnel, error) {
	ctx, span := h.tracer.Start(ctx, "findHandle.Get", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	select {
	case <-ctx.Done():
		span.SetStatus(otelcodes.Error, "FindTunnel request aborted")
		span.RecordError(ctx.Err())
		return nil, grpctool.StatusErrorFromContext(ctx, "FindTunnel request aborted")
	case tun := <-h.retTun:
		h.gotTunnel = true
		if tun == nil {
			span.SetStatus(otelcodes.Error, "kas is shutting down")
			return nil, status.Error(codes.Unavailable, "kas is shutting down")
		}
		span.SetStatus(otelcodes.Ok, "")
		return tun, nil
	}
}

func (h *findHandle) Done(ctx context.Context) {
	ctx, span := h.tracer.Start(ctx, "findHandle.Done", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	if h.gotTunnel {
		// No cleanup needed if Get returned a tunnel.
		return
	}
	h.done(ctx)
}

type agentID2tunInfo struct {
	tuns map[*tunserver.TunnelImpl]struct{}
}

type registryStripe struct {
	log           *zap.Logger
	api           modshared.API
	tracer        trace.Tracer
	tunnelTracker Tracker
	ttl           time.Duration

	mu                    sync.Mutex
	tunsByAgentID         map[int64]agentID2tunInfo
	findRequestsByAgentID map[int64]map[*findTunnelRequest]struct{}
}

func (r *registryStripe) Refresh(ctx context.Context) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	toRefresh := r.agentIDsLocked()
	return r.tunnelTracker.Refresh(ctx, r.ttl, toRefresh)
}

func (r *registryStripe) GC(ctx context.Context) (int /* tunnelsDeleted */, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	tunsToGC := r.agentIDsLocked()
	return r.tunnelTracker.GC(ctx, tunsToGC)
}

func (r *registryStripe) agentIDsLocked() []int64 {
	ids := make([]int64, 0, len(r.tunsByAgentID))
	for agentID := range r.tunsByAgentID {
		ids = append(ids, agentID)
	}
	return ids
}

func (r *registryStripe) FindTunnel(ctx context.Context, agentID int64, service, method string) (bool, tunserver.FindHandle) {
	ctx, span := r.tracer.Start(ctx, "registryStripe.FindTunnel", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	// Buffer 1 to not block on send when a tunnel is found before find request is registered.
	retTun := make(chan *tunserver.TunnelImpl, 1) // can receive nil from it if Stop() is called
	ftr := &findTunnelRequest{
		agentID: agentID,
		service: service,
		method:  method,
		retTun:  retTun,
	}
	found := false
	func() {
		r.mu.Lock()
		defer r.mu.Unlock()

		// 1. Check if we have a suitable tunnel
		for tun := range r.tunsByAgentID[agentID].tuns {
			if !tun.Descriptor.SupportsServiceAndMethod(service, method) {
				continue
			}
			// Suitable tunnel found!
			tun.State = tunserver.StateFound
			retTun <- tun // must not block because the reception is below
			found = true
			r.unregisterTunnelLocked(ctx, tun)
			return
		}
		// 2. No suitable tunnel found, add to the queue
		findRequestsForAgentID := r.findRequestsByAgentID[agentID]
		if findRequestsForAgentID == nil {
			findRequestsForAgentID = make(map[*findTunnelRequest]struct{}, 1)
			r.findRequestsByAgentID[agentID] = findRequestsForAgentID
		}
		findRequestsForAgentID[ftr] = struct{}{}
	}()
	span.SetAttributes(traceTunnelFoundAttr.Bool(found))
	return found, &findHandle{
		tracer: r.tracer,
		retTun: retTun,
		done: func(ctx context.Context) {
			r.mu.Lock()
			defer r.mu.Unlock()
			close(retTun)
			tun := <-retTun // will get nil if there was nothing in the channel or if registry is shutting down.
			if tun != nil {
				// Got the tunnel, but it's too late so return it to the registry.
				r.onTunnelDoneLocked(ctx, tun)
			} else {
				r.deleteFindRequestLocked(ftr)
			}
		},
	}
}

func (r *registryStripe) HandleTunnel(ageCtx context.Context, agentInfo *api.AgentInfo, server rpc.ReverseTunnel_ConnectServer) error {
	ctx := server.Context()
	ctx, span := r.tracer.Start(ctx, "registryStripe.HandleTunnel", trace.WithSpanKind(trace.SpanKindServer))
	defer span.End() // we don't add the returned error to the span as it's added by the gRPC OTEL stats handler already.

	recv, err := server.Recv()
	if err != nil {
		return err
	}
	descriptor, ok := recv.Msg.(*rpc.ConnectRequest_Descriptor_)
	if !ok {
		return status.Errorf(codes.InvalidArgument, "invalid oneof value type: %T", recv.Msg)
	}
	retErr := make(chan error, 1)
	agentID := agentInfo.ID
	tun := &tunserver.TunnelImpl{
		Tunnel:       server,
		TunnelRetErr: retErr,
		AgentID:      agentID,
		Descriptor:   descriptor.Descriptor_.ApiDescriptor,
		State:        tunserver.StateReady,
		OnForward:    r.onTunnelForward,
		OnDone:       r.onTunnelDone,
	}
	// Register
	r.registerTunnel(ctx, tun) //nolint: contextcheck
	// Wait for return error or for cancellation
	select {
	case <-ageCtx.Done():
		// Context canceled
		r.mu.Lock()
		switch tun.State {
		case tunserver.StateReady:
			tun.State = tunserver.StateContextDone
			r.unregisterTunnelLocked(ctx, tun) //nolint: contextcheck
			r.mu.Unlock()
			return nil
		case tunserver.StateFound:
			// Tunnel was found but hasn't been used yet, Done() hasn't been called.
			// Set State to StateContextDone so that ForwardStream() errors out without doing any I/O.
			tun.State = tunserver.StateContextDone
			r.mu.Unlock()
			return nil
		case tunserver.StateForwarding:
			// I/O on the stream will error out, just wait for the return value.
			r.mu.Unlock()
			return <-retErr
		case tunserver.StateDone:
			// Forwarding has finished and then ctx signaled done. Return the result value from forwarding.
			r.mu.Unlock()
			return <-retErr
		case tunserver.StateContextDone:
			// Cannot happen twice.
			r.mu.Unlock()
			panic(errors.New("unreachable"))
		default:
			// Should never happen
			r.mu.Unlock()
			panic(fmt.Errorf("invalid State: %d", tun.State))
		}
	case err = <-retErr:
		return err
	}
}

func (r *registryStripe) registerTunnel(ctx context.Context, toReg *tunserver.TunnelImpl) {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.registerTunnelLocked(ctx, toReg)
}

func (r *registryStripe) registerTunnelLocked(ctx context.Context, toReg *tunserver.TunnelImpl) {
	agentID := toReg.AgentID
	// 1. Before registering the tunnel see if there is a find tunnel request waiting for it
	findRequestsForAgentID := r.findRequestsByAgentID[agentID]
	for ftr := range findRequestsForAgentID {
		if !toReg.Descriptor.SupportsServiceAndMethod(ftr.service, ftr.method) {
			continue
		}
		// Waiting request found!
		toReg.State = tunserver.StateFound
		// NOTE: this is temporary set from Debug to Info in the scope of
		// https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/585
		r.log.Info("Registering agent tunnel and immediately found request to satisfy", logz.AgentID(agentID), logz.TunnelsByAgent(len(r.tunsByAgentID[agentID].tuns)))

		ftr.retTun <- toReg            // Satisfy the waiting request ASAP
		r.deleteFindRequestLocked(ftr) // Remove it from the queue
		return
	}

	// NOTE: this is temporary set from Debug to Info in the scope of
	// https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/585
	r.log.Info("Registering agent tunnel", logz.AgentID(agentID), logz.TunnelsByAgent(len(r.tunsByAgentID[agentID].tuns)))

	// 2. Register the tunnel
	toReg.State = tunserver.StateReady
	info, ok := r.tunsByAgentID[agentID]
	if !ok {
		info = agentID2tunInfo{
			tuns: make(map[*tunserver.TunnelImpl]struct{}),
		}
		r.tunsByAgentID[agentID] = info // not a pointer, put it in
		// First tunnel for this agentID. Register it.
		// Don't pass the original context to always register.
		err := r.tunnelTracker.RegisterTunnel(context.WithoutCancel(ctx), r.ttl, agentID)
		if err != nil {
			r.api.HandleProcessingError(ctx, r.log.With(logz.AgentID(agentID)), "Failed to register tunnel", err, fieldz.AgentID(agentID))
		}
	}
	info.tuns[toReg] = struct{}{}
}

func (r *registryStripe) unregisterTunnelLocked(ctx context.Context, toUnreg *tunserver.TunnelImpl) {
	agentID := toUnreg.AgentID
	info := r.tunsByAgentID[agentID]
	delete(info.tuns, toUnreg)

	// NOTE: this is temporary set from Debug to Info in the scope of
	// https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/585
	r.log.Info("Unregistering agent tunnel", logz.AgentID(agentID), logz.TunnelsByAgent(len(info.tuns)))
	if len(info.tuns) > 0 {
		// There are more tunnels for this agent id, nothing to do.
		return
	}
	// Last tunnel for this agentID had been used. Unregister it.
	delete(r.tunsByAgentID, agentID)
	r.unregisterTunnelIO(ctx, agentID)
}

func (r *registryStripe) unregisterTunnelIO(ctx context.Context, agentID int64) {
	// Don't pass the original context to always unregister
	err := r.tunnelTracker.UnregisterTunnel(context.WithoutCancel(ctx), agentID)
	if err != nil {
		r.api.HandleProcessingError(ctx, r.log.With(logz.AgentID(agentID)), "Failed to unregister tunnel", err, fieldz.AgentID(agentID))
	}
}

func (r *registryStripe) onTunnelForward(tun *tunserver.TunnelImpl) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	switch tun.State {
	case tunserver.StateReady:
		return status.Error(codes.Internal, "unreachable: ready -> forwarding should never happen")
	case tunserver.StateFound:
		tun.State = tunserver.StateForwarding
		return nil
	case tunserver.StateForwarding:
		return status.Error(codes.Internal, "ForwardStream() called more than once")
	case tunserver.StateDone:
		return status.Error(codes.Internal, "ForwardStream() called after Done()")
	case tunserver.StateContextDone:
		return status.Error(codes.Canceled, "ForwardStream() called on done stream")
	default:
		return status.Errorf(codes.Internal, "unreachable: invalid State: %d", tun.State)
	}
}

func (r *registryStripe) onTunnelDone(ctx context.Context, tun *tunserver.TunnelImpl) {
	ctx, span := r.tracer.Start(ctx, "registryStripe.onTunnelDone", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	r.mu.Lock()
	defer r.mu.Unlock()
	r.onTunnelDoneLocked(ctx, tun)
}

func (r *registryStripe) onTunnelDoneLocked(ctx context.Context, tun *tunserver.TunnelImpl) {
	switch tun.State {
	case tunserver.StateReady:
		panic(errors.New("unreachable: ready -> done should never happen"))
	case tunserver.StateFound:
		// Tunnel was found but was not used, Done() was called. Just put it back.
		r.registerTunnelLocked(ctx, tun)
	case tunserver.StateForwarding:
		tun.State = tunserver.StateDone
	case tunserver.StateDone:
		panic(errors.New("Done() called more than once"))
	case tunserver.StateContextDone:
	// Done() called after canceled context in HandleTunnel(). Nothing to do.
	default:
		// Should never happen
		panic(fmt.Errorf("invalid State: %d", tun.State))
	}
}

func (r *registryStripe) deleteFindRequestLocked(ftr *findTunnelRequest) {
	findRequestsForAgentID := r.findRequestsByAgentID[ftr.agentID]
	delete(findRequestsForAgentID, ftr)
	if len(findRequestsForAgentID) == 0 {
		delete(r.findRequestsByAgentID, ftr.agentID)
	}
}

// Stop aborts any open tunnels.
// It should not be necessary to abort tunnels when registry is used correctly i.e. this method is called after
// all tunnels have terminated gracefully.
func (r *registryStripe) Stop(ctx context.Context) (int /*stoppedTun*/, int /*abortedFtr*/) {
	stoppedTun := 0
	abortedFtr := 0

	r.mu.Lock()
	defer r.mu.Unlock()

	// 1. Abort all waiting new stream requests
	for _, findRequestsForAgentID := range r.findRequestsByAgentID {
		for ftr := range findRequestsForAgentID {
			ftr.retTun <- nil
		}
		abortedFtr += len(findRequestsForAgentID)
	}
	clear(r.findRequestsByAgentID)

	// 2. Abort all tunnels
	for agentID, info := range r.tunsByAgentID {
		for tun := range info.tuns {
			tun.State = tunserver.StateDone
			tun.TunnelRetErr <- nil // nil so that HandleTunnel() returns cleanly and agent immediately retries
		}
		stoppedTun += len(info.tuns)
		r.unregisterTunnelIO(ctx, agentID)
	}
	clear(r.tunsByAgentID)

	if stoppedTun > 0 || abortedFtr > 0 {
		r.api.HandleProcessingError(ctx, r.log, "", errors.New("stopped tunnels or aborted find tunnel requests"),
			fieldz.NewField("num_stopped_tunnels", stoppedTun), fieldz.NewField("num_aborted_find_tunnel_requests", abortedFtr))
	}
	return stoppedTun, abortedFtr
}
