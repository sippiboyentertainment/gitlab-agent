package manifestops

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
	"go.uber.org/zap"
)

type module struct {
	log *zap.Logger
}

func (m *module) Run(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
	for config := range cfg {
		if config.Gitops != nil {
			m.log.Error("The GitOps module is configured in the agent configuration file, but the module was removed in GitLab 17.0. For more information, see https://gitlab.com/gitlab-org/gitlab/-/issues/406545. You should migrate to Flux by following the steps in https://docs.gitlab.com/ee/user/clusters/agent/gitops/migrate_to_flux.html.")
		}
	}
	return nil
}

func (m *module) DefaultAndValidateConfiguration(config *agentcfg.AgentConfiguration) error {
	return nil
}

func (m *module) Name() string {
	return "gitops-manifest"
}
