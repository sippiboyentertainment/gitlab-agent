package kasapp

import (
	"context"

	agentk2kas_tunnel_router "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/router"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type agentConnPool struct {
	routingConn grpc.ClientConnInterface
}

func newAgentConnPool(routingConn grpc.ClientConnInterface) *agentConnPool {
	return &agentConnPool{
		routingConn: routingConn,
	}
}

func (p *agentConnPool) Get(agentID int64) grpc.ClientConnInterface {
	return &routingMetadataWrapper{
		delegate: p.routingConn,
		agentID:  agentID,
	}
}

var (
	_ grpc.ClientConnInterface = (*routingMetadataWrapper)(nil)
)

type routingMetadataWrapper struct {
	delegate grpc.ClientConnInterface
	agentID  int64
}

func (w *routingMetadataWrapper) Invoke(ctx context.Context, method string, args any, reply any, opts ...grpc.CallOption) error {
	return w.delegate.Invoke(w.setRoutingMetadata(ctx), method, args, reply, opts...)
}

func (w *routingMetadataWrapper) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return w.delegate.NewStream(w.setRoutingMetadata(ctx), desc, method, opts...)
}

func (w *routingMetadataWrapper) setRoutingMetadata(ctx context.Context) context.Context {
	md, _ := metadata.FromOutgoingContext(ctx)
	md = agentk2kas_tunnel_router.SetRoutingMetadata(md, w.agentID)
	return metadata.NewOutgoingContext(ctx, md)
}
