package agentkapp

import (
	"os"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
	"go.uber.org/zap"
	"go.uber.org/zap/buffer"
	"go.uber.org/zap/zapcore"
)

func (a *App) logger(envVar string, levelEnum agentcfg.LogLevelEnum, sync zapcore.WriteSyncer) (*zap.Logger, zap.AtomicLevel, error) {
	l := os.Getenv(envVar)
	if l == "" {
		l = levelEnum.String()
	}
	level, err := logz.LevelFromString(l)
	if err != nil {
		return nil, zap.NewAtomicLevel(), err
	}
	atomicLevel := zap.NewAtomicLevelAt(level)
	return zap.New(
		zapcore.NewCore(
			&agentIDEncoder{
				Encoder: zapcore.NewJSONEncoder(logz.NewProductionEncoderConfig()),
				agentID: a.AgentID,
			},
			sync,
			atomicLevel,
		),
		zap.ErrorOutput(sync),
	), atomicLevel, nil
}

// agentIDEncoder wraps a zapcore.Encoder to add agent id field if agent id is available.
type agentIDEncoder struct {
	zapcore.Encoder
	agentID *ValueHolder[int64]
}

func (e *agentIDEncoder) EncodeEntry(entry zapcore.Entry, fields []zapcore.Field) (*buffer.Buffer, error) {
	id, ok := e.agentID.tryGet()
	if ok {
		l := len(fields)
		f := make([]zapcore.Field, l+1)
		copy(f, fields)
		f[l] = logz.AgentID(id)
		fields = f
	}
	return e.Encoder.EncodeEntry(entry, fields)
}

func (e *agentIDEncoder) Clone() zapcore.Encoder {
	return &agentIDEncoder{
		Encoder: e.Encoder.Clone(),
		agentID: e.agentID,
	}
}
