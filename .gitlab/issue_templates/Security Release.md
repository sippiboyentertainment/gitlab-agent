<!--
# Read me first!

Create this issue under https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent

Set the title to: `Description of the original issue`

Also have a look at GitLab Agent specific documentation at:

- https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/security_patch.md
- https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/backports.md

-->

## Prior to starting the security release work

- [ ] Read the [security process for developers] if you are not familiar with it.
- [ ] Run `scripts/security-harness` in your local repository to prevent accidentally pushing to any remote besides `gitlab.com/gitlab-org/security`.
- Fill out the [Links section](#links):
  - [ ] Next to **Issue on GitLab Agent**, add a link to the `gitlab-org/cluster-integration/gitlab-agent` issue that describes the security vulnerability.

## Development

- [ ] Create a new branch prefixing it with `security-`.
- [ ] Create a merge request targeting `master` on `gitlab-org/cluster-integration/gitlab-agent` and use the [Security Release merge request template].
- [ ] Follow the same [code review] process: Assign to a reviewer, then to a maintainer.

After your merge request has been approved according to our [approval guidelines], you're ready to prepare the backports

## Backports

- [ ] Once the MR is ready to be merged, create MRs targeting the latest 3 stable branches
   * The 3 stable branches correspond to the versions in the title of the [Security Tracking Issue].
   * At this point, it might be easy to squash the commits from the MR into one
   * You can use the script `bin/secpick` instead of the following steps, to help you cherry-picking. See the [secpick documentation]
- [ ] Create each MR targeting the stable branch `X-Y-stable`, using the [Security Release merge request template].
   * Every merge request will have its own set of TODOs, so make sure to complete those.
- [ ] On the "Related merge requests" section, ensure all MRs are linked to this issue.
   * This section should only list the merge requests created for this issue: One targeting `master` and the 3 backports.
- [ ] If this issue requires less than `4` merge requests, add the ~"reduced backports" label.

## Assigning to a release

- [ ] When this issue is ready for release (Backports are approved and ready to be merged), apply the ~"security-target" label for it to be evaluated and linked to the next planned security release tracking issue.

## Documentation and final details

- [ ] Ensure the [Links section](#links) is completed.
- [ ] Add the GitLab [versions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/engineer.md#versions-affected) and editions affected to the [details section](#details)
  * The Git history of the files affected may help you associate the issue with a [release](https://about.gitlab.com/releases/)
- [ ] Fill in any upgrade notes that users may need to take into account in the [details section](#details)
- [ ] Add the nickname of the external user who found the issue (and/or HackerOne profile) to the Thanks row in the [details section](#details)

## Summary

### Links

| Description | Link |
| -------- | -------- |
| Issue on [GitLab Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/issues) | #TODO  |

### Details

| Description | Details | Further details|
| -------- | -------- | -------- |
| Versions affected | X.Y  | |
| Upgrade notes | | |
| Thanks | | |

[security process for developers]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/engineer.md
[secpick documentation]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/utilities/secpick_script.md
[security Release merge request template]: https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/blob/master/.gitlab/merge_request_templates/Security%20Release.md
[code review process]: https://docs.gitlab.com/ee/development/code_review.html
[approval guidelines]: https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines
[issue as related]: https://docs.gitlab.com/ee/user/project/issues/related_issues.html#adding-a-related-issue
[Security Tracking Issue]: https://gitlab.com/gitlab-org/gitlab/-/issues/?label_name%5B%5D=upcoming%20security%20release

/label ~"group::environments" ~"security" ~"security-notifications"
